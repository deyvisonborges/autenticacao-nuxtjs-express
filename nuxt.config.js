import bodyParser from 'body-parser'
import session from 'express-session'

export default {
  head: {
    title: 'Auth Routes',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', content: 'Autenticação com Nuxt.js e Express' }
    ]
  },

  serverMiddleware: [
    bodyParser.json(),
    session({
      secret: '32c30c28y3t58C!479643276154',
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 60000 }
    }),
    '~/api'
  ]
}
