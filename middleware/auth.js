export default function ({ store, error }) {
  if (!store.state.authUser) {
    error({
      message: 'Você não está autenticado',
      statusCode: 403
    })
  }
}
